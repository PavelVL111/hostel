<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Pavel
  Date: 25.05.2017
  Time: 12:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Выселяющиеся</title>
</head>
<body>
<div align="center">
    <h1>Выселяющиеся</h1>
    <table border="1">
        <th>№</th>
        <th>ФИО</th>
        <th>Пол</th>
        <th>Телефон</th>
        <th>Счет</th>
         <c:forEach var="visitor" items="${listVisitor}" varStatus="status">
            <tr>
                <td>${status.index + 1}</td>
                <td>${visitor.name}</td>
                <td>${visitor.gender}</td>
                <td>${visitor.telephone}</td>
                <td>${visitor.cost} $</td>
            </tr>
        </c:forEach>
    </table>
    <h3><a href="javascript:history.back()">Главная</a></h3>
</div>
</body>
</html>
