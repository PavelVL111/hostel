<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Pavel
  Date: 25.05.2017
  Time: 17:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Загрузка комнат</title>
</head>
<body>
<div align="center">
    <h1>Загрузка комнат</h1>
    <table border="1">
        <th>Номер комнаты/Кол-во мест</th>
        <th>&nbsp;&nbsp;1&nbsp;&nbsp;</th>
        <th>&nbsp;&nbsp;2&nbsp;&nbsp;</th>
        <th>&nbsp;&nbsp;3&nbsp;&nbsp;</th>
        <th>&nbsp;&nbsp;4&nbsp;&nbsp;</th>
        <th>&nbsp;&nbsp;5&nbsp;&nbsp;</th>
        <c:forEach var="rooms" items="${rooms}" varStatus="status">
            <tr>
                <td>${rooms.roomNumber}</td>
                <td>${rooms.one}</td>
                <td>${rooms.two}</td>
                <td>${rooms.three}</td>
                <td>${rooms.four}</td>
                <td>${rooms.five}</td>
            </tr>
        </c:forEach>
    </table>
    <h3><a href="javascript:history.back()">Главная</a></h3>
</div>
</body>
</html>
