<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New/Edit Reserve</title>
</head>
<body>
	<div align="center">
		<h1>Добавить/Редактировать</h1>
		<form:form action="saveReserve" method="post" modelAttribute="reserve">
		<table>
			<form:hidden path="reserveId"/>
			<tr>
				<td>Дата заселения:</td>
				<td><form:input path="dateIn" /></td>
			</tr>
			<tr>
				<td>Дата выселения:</td>
				<td><form:input path="dateOut" /></td>
			</tr>
			<tr>
				<td>Номер комнаты:</td>
				<td><form:input path="roomNumber" /></td>
			</tr>
			<tr>
				<td>ФИО Посетителя:</td>
				<td><form:input path="visitorName" /></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" value="Save"></td>
			</tr>
		</table>
		</form:form>
	</div>
</body>
</html>