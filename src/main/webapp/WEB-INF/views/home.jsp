<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hostel</title>
    </head>
    <body>

    	<div align="center">
	        <h1>Список посетителей</h1>
	        <table border="1">
	        	<th>№</th>
	        	<th>ФИО</th>
	        	<th>Пол</th>
	        	<th>Телефон</th>
	        	<th>Изменить</th>

				<c:forEach var="visitor" items="${listVisitor}" varStatus="status">
	        	<tr>
	        		<td>${status.index + 1}</td>
					<td>${visitor.name}</td>
					<td>${visitor.gender}</td>
					<td>${visitor.telephone}</td>
					<td>
						<a href="editVisitor?visitorId=${visitor.visitorId}">Редактировать</a>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="deleteVisitor?visitorId=${visitor.visitorId}">Удалить</a>
					</td>

	        	</tr>
				</c:forEach>
			</table>
			<h3><a href="newVisitor">Добавить посетителя</a></h3>
    	</div>
		<hr>
		<div align="center">
			<h1>Список комнат</h1>
			<table border="1">
				<th>№</th>
				<th>Номер комнаты</th>
				<th>Цена/День</th>
				<th>Кол-во мест</th>
				<th>Кол-во занятых мест</th>
				<th>Класс</th>
				<th>Изменить</th>

				<c:forEach var="room" items="${listRoom}" varStatus="status">
					<tr>
						<td>${status.index + 1}</td>
						<td>${room.roomNumber}</td>
						<td>${room.cost} $</td>
						<td>${room.places}</td>
						<td>${room.occupiedPlaces}</td>
						<td>${room.serviceLevel}</td>
						<td>
							<a href="editRoom?roomId=${room.roomId}">Редактировать</a>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="deleteRoom?roomId=${room.roomId}">Удалить</a>
						</td>

					</tr>
				</c:forEach>
			</table>
			<h3><a href="newRoom">Добавить комнату</a></h3>
		</div>
		<hr>
		<div align="center">
			<h1>Список резерваций</h1>
			<table border="1">
				<th>№</th>
				<th>Дата заселения</th>
				<th>День выселения</th>
				<th>Номнр комнаты</th>
				<th>ФИО Посетителя</th>
				<th>Изменить</th>

				<c:forEach var="reserve" items="${listReserve}" varStatus="status">
					<tr>
						<td>${status.index + 1}</td>
						<td>${reserve.dateIn}</td>
						<td>${reserve.dateOut}</td>
						<td>${reserve.roomNumber}</td>
						<td>${reserve.visitorName}</td>
						<td>
							<a href="editReserve?reserveId=${reserve.reserveId}">Редактировать</a>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="deleteReserve?reserveId=${reserve.reserveId}">Удалить</a>
						</td>

					</tr>
				</c:forEach>
			</table>
			<h3><a href="newReserve">Новая резервация</a></h3>
			<hr>
			<h3><a href="outVisitors">Выселяющиеся</a></h3>
			<h3><a href="loadRoom">Загрузка комнат</a></h3>
		</div>
    </body>
</html>
