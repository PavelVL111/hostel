<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New/Edit Room</title>
</head>
<body>
	<div align="center">
		<h1>Добавить/Редактировать</h1>
		<form:form action="saveRoom" method="post" modelAttribute="room">
		<table>
			<form:hidden path="roomId"/>
			<tr>
				<td>Номер комнаты:</td>
				<td><form:input path="roomNumber" /></td>
			</tr>
			<tr>
				<td>Цена в сутки:</td>
				<td><form:input path="cost" /></td>
			</tr>
			<tr>
				<td>Кол-во мест:</td>
				<td><form:input path="places" /></td>
			</tr>
			<tr>
				<td>Кол-во занятых мест:</td>
				<td><form:input path="occupiedPlaces" /></td>
			</tr>
			<tr>
				<td>Класс комнаты:</td>
				<td><form:input path="serviceLevel" /></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" value="Save"></td>
			</tr>
		</table>
		</form:form>
	</div>
</body>
</html>