package net.codejava.spring.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.codejava.spring.dao.*;
import net.codejava.spring.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller routes accesses to the application to the appropriate
 * hanlder methods. 
 * @author www.codejava.net
 *
 */
@Controller
public class HomeController {


	@Autowired
	private VisitorDAO visitorDAO;

	@Autowired
	private RoomDAO roomDAO;

	@Autowired
	private ReserveDAO reserveDAO;



	@RequestMapping(value="/")
	public ModelAndView listVisitor(ModelAndView model) throws IOException{
		List<Visitor> listVisitor = visitorDAO.list();
		model.addObject("listVisitor", listVisitor);
		List<Room> listRoom = roomDAO.list();
		model.addObject("listRoom", listRoom);
		List<Reserve> listReserve = reserveDAO.list();
		model.addObject("listReserve", listReserve);
		model.setViewName("home");
		return model;
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@RequestMapping(value = "/newVisitor", method = RequestMethod.GET)
	public ModelAndView newVisitor(ModelAndView model) {
		Visitor newVisitor = new Visitor();
		model.addObject("visitor", newVisitor);
		model.setViewName("VisitorForm");
		return model;
	}

	@RequestMapping(value = "/saveVisitor", method = RequestMethod.POST)
	public ModelAndView saveVisitor(@ModelAttribute Visitor visitor) {
		visitorDAO.saveOrUpdate(visitor);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/deleteVisitor", method = RequestMethod.GET)
	public ModelAndView deleteVisitor(HttpServletRequest request) {
		int visitorId = Integer.parseInt(request.getParameter("visitorId"));
		visitorDAO.delete(visitorId);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/editVisitor", method = RequestMethod.GET)
	public ModelAndView editVisitor(HttpServletRequest request) {
		int visitorId = Integer.parseInt(request.getParameter("visitorId"));
		Visitor visitor = visitorDAO.get(visitorId);
		ModelAndView model = new ModelAndView("VisitorForm");
		model.addObject("visitor", visitor);

		return model;
	}

	////////////////////////////////////////////////////
	@RequestMapping(value = "/newRoom", method = RequestMethod.GET)
	public ModelAndView newRoom(ModelAndView model) {
		Room newRoom = new Room();
		model.addObject("room", newRoom);
		model.setViewName("RoomForm");
		return model;
	}

	@RequestMapping(value = "/saveRoom", method = RequestMethod.POST)
	public ModelAndView saveRoom(@ModelAttribute Room room) {
		roomDAO.saveOrUpdate(room);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/deleteRoom", method = RequestMethod.GET)
	public ModelAndView deleteRoom(HttpServletRequest request) {
		int roomId = Integer.parseInt(request.getParameter("roomId"));
		roomDAO.delete(roomId);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/editRoom", method = RequestMethod.GET)
	public ModelAndView editRoom(HttpServletRequest request) {
		int roomId = Integer.parseInt(request.getParameter("roomId"));
		Room room = roomDAO.get(roomId);
		ModelAndView model = new ModelAndView("RoomForm");
		model.addObject("room", room);

		return model;
	}

	///////////////////////////////////////////////////////////////////////
	@RequestMapping(value = "/newReserve", method = RequestMethod.GET)
	public ModelAndView newReserve(ModelAndView model) {
		Reserve newReserve = new Reserve();
		model.addObject("reserve", newReserve);
		model.setViewName("ReserveForm");
		return model;
	}

	@RequestMapping(value = "/saveReserve", method = RequestMethod.POST)
	public ModelAndView saveReserve(@ModelAttribute Reserve reserve) {
		reserveDAO.saveOrUpdate(reserve);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/deleteReserve", method = RequestMethod.GET)
	public ModelAndView deleteReserve(HttpServletRequest request) {
		int reserveId = Integer.parseInt(request.getParameter("reserveId"));
		reserveDAO.delete(reserveId);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/editReserve", method = RequestMethod.GET)
	public ModelAndView editReserve(HttpServletRequest request) {
		int reserveId = Integer.parseInt(request.getParameter("reserveId"));
		Reserve reserve = reserveDAO.get(reserveId);
		ModelAndView model = new ModelAndView("ReserveForm");
		model.addObject("reserve", reserve);

		return model;
	}

	@RequestMapping(value = "/outVisitors")
	public ModelAndView outVisitors(ModelAndView model) throws IOException{
		List<OutVisitor> listVisitor = visitorDAO.listOutVisitor();
		model.addObject("listVisitor", listVisitor);
		model.setViewName("OutVisitors");
		return model;
	}

	@RequestMapping(value = "/loadRoom")
	public ModelAndView loadRoom(ModelAndView model) throws IOException{
		List<LoadRoom> listRoom = roomDAO.loadRoom();
		model.addObject("rooms", listRoom);
		model.setViewName("LoadRoom");
		return model;
	}

}
