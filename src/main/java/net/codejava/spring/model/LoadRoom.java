package net.codejava.spring.model;

public class LoadRoom {
	private int roomNumber;
	//private int total;
	private int one;
	private int two;
	private int three;
	private int four;
	private int five;

	public LoadRoom() {
	}

	public LoadRoom(int roomNumber, int one, int two, int three, int four, int five) {
		this.roomNumber = roomNumber;
		//this.total = total;
		this.one = one;
		this.two = two;
		this.three = three;
		this.four = four;
		this.five = five;
	}

//	public int getTotal() {
//		return total;
//	}

//	public void setTotal(int total) {
//		this.total = total;
//	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getOne() {
		return one;
	}

	public void setOne(int one) {
		this.one = one;
	}

	public int getTwo() {
		return two;
	}

	public void setTwo(int two) {
		this.two = two;
	}

	public int getThree() {
		return three;
	}

	public void setThree(int three) {
		this.three = three;
	}

	public int getFour() {
		return four;
	}

	public void setFour(int four) {
		this.four = four;
	}

	public int getFive() {
		return five;
	}

	public void setFive(int five) {
		this.five = five;
	}
}
