package net.codejava.spring.model;

public class Visitor {
	private int visitorId;
	private String name;
	private String gender;
	private String telephone;

	public Visitor() {
	}

	public Visitor(String name, String gender, String telephone) {
		this.name = name;
		this.gender = gender;
		this.telephone = telephone;
	}

	public int getVisitorId() {
		return visitorId;
	}

	public void setVisitorId(int visitorId) {
		this.visitorId = visitorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
