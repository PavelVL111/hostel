package net.codejava.spring.model;

public class OutVisitor {
	private int visitorId;
	private String name;
	private String gender;
	private String photo;
	private String telephone;
	private int cost;

	public OutVisitor() {
	}

	public OutVisitor(String name, String gender, String photo, String telephone, int cost) {
		this.name = name;
		this.gender = gender;
		this.photo = photo;
		this.telephone = telephone;
		this.cost = cost;
	}

	public int getVisitorId() {
		return visitorId;
	}

	public void setVisitorId(int visitorId) {
		this.visitorId = visitorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
