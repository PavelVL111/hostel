package net.codejava.spring.model;

import java.sql.Date;

public class Reserve {
	private int reserveId;
	private Date dateIn;
	private Date dateOut;
	private int roomNumber;
	private String visitorName;

	public Reserve() {
	}

	public Reserve(Date dateIn, Date dateOut, int roomNumber, String visitorName) {
		this.dateIn = dateIn;
		this.dateOut = dateOut;
		this.roomNumber = roomNumber;
		this.visitorName = visitorName;
	}

	public int getReserveId() {
		return reserveId;
	}

	public void setReserveId(int reserveId) {
		this.reserveId = reserveId;
	}

	public Date getDateIn() {
		return dateIn;
	}

	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}

	public Date getDateOut() {
		return dateOut;
	}

	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getVisitorName() {
		return visitorName;
	}

	public void setVisitorName(String visitorName) {
		this.visitorName = visitorName;
	}
}
