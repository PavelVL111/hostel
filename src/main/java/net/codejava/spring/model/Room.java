package net.codejava.spring.model;

public class Room {
	private int roomId;
	private int roomNumber;
	private int cost;
	private int places;
	private int occupiedPlaces;
	private String serviceLevel;

	public Room() {
	}

	public Room(int roomNumber, int cost, String serviceLevel, int places, int occupiedPlaces) {
		this.roomNumber = roomNumber;
		this.cost = cost;
		this.places = places;
		this.occupiedPlaces = occupiedPlaces;
		this.serviceLevel = serviceLevel;
	}

	public int getRoomId() {
		return roomId;
	}

	public int getPlaces() {
		return places;
	}

	public void setPlaces(int places) {
		this.places = places;
	}

	public void setOccupiedPlaces(int occupiedPlaces) {
		this.occupiedPlaces = occupiedPlaces;
	}

	public int getOccupiedPlaces() {

		return occupiedPlaces;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getServiceLevel() {
		return serviceLevel;
	}

	public void setServiceLevel(String serviceLevel) {
		this.serviceLevel = serviceLevel;
	}
}
