package net.codejava.spring.dao;

import net.codejava.spring.model.LoadRoom;
import net.codejava.spring.model.Room;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * An implementation of the RoomDAO interface.
 * @author www.codejava.net
 *
 */
public class RoomDAOImpl implements RoomDAO {

	private JdbcTemplate jdbcTemplate;
	
	public RoomDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void saveOrUpdate(Room room) {
		if (room.getRoomId() > 0) {
			// update
			String sql = "UPDATE rooms SET roomNumber=?, cost=?, occupied_places=?, places=?, serviceLevel=?"
						+ " WHERE roomId=?";
			jdbcTemplate.update(sql, room.getRoomNumber(), room.getCost(),
					room.getOccupiedPlaces(), room.getPlaces(), room.getServiceLevel(), room.getRoomId());
		} else {
			// insert
			String sql = "INSERT INTO rooms (roomNumber, cost, serviceLevel, places, occupied_places)"
						+ " VALUES (?, ?, ?, ?, ?)";
			jdbcTemplate.update(sql, room.getRoomNumber(), room.getCost(),
					room.getServiceLevel(), room.getPlaces(), room.getOccupiedPlaces());
		}
		
	}

	@Override
	public void delete(int roomId) {
		String sql2 = "DELETE FROM reservations WHERE roomId=?";
		jdbcTemplate.update(sql2, roomId);
		String sql = "DELETE FROM rooms WHERE roomId=?";
		jdbcTemplate.update(sql, roomId);
	}

	@Override
	public List<Room> list() {
		String sql = "SELECT * FROM rooms";
		List<Room> listRoom = jdbcTemplate.query(sql, new RowMapper<Room>() {

			@Override
			public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
				Room aRoom = new Room();

				aRoom.setRoomId(rs.getInt("roomId"));
				aRoom.setRoomNumber(rs.getInt("roomNumber"));
				aRoom.setCost(rs.getInt("cost"));
				aRoom.setPlaces(rs.getInt("places"));
				aRoom.setOccupiedPlaces(rs.getInt("occupied_places"));
				aRoom.setServiceLevel(rs.getString("serviceLevel"));
				
				return aRoom;
			}
			
		});
		
		return listRoom;
	}

	@Override
	public Room get(int roomId) {
		String sql = "SELECT * FROM rooms WHERE roomId=" + roomId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Room>() {

			@Override
			public Room extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Room room = new Room();
					room.setRoomId(rs.getInt("roomId"));
					room.setRoomNumber(rs.getInt("roomNumber"));
					room.setCost(rs.getInt("cost"));
					room.setPlaces(rs.getInt("places"));
					room.setOccupiedPlaces(rs.getInt("occupied_places"));
					room.setServiceLevel(rs.getString("serviceLevel"));
					return room;
				}
				
				return null;
			}
			
		});
	}

	@Override
	public Room getByNumber(int roomNumber) {
		String sql = "SELECT * FROM rooms WHERE roomNumber=" + roomNumber;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Room>() {

			@Override
			public Room extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Room room = new Room();
					room.setRoomId(rs.getInt("roomId"));
					room.setRoomNumber(rs.getInt("roomNumber"));
					room.setCost(rs.getInt("cost"));
					room.setPlaces(rs.getInt("places"));
					room.setOccupiedPlaces(rs.getInt("occupied_places"));
					room.setServiceLevel(rs.getString("serviceLevel"));
					return room;
				}

				return null;
			}

		});
	}

	@Override
	public List<LoadRoom> loadRoom() {
		String sql = "SELECT rooms.roomNumber, " +
				"(CASE places WHEN '1' THEN occupied_places ELSE 0 END) AS '1', " +
				"(CASE places WHEN '2' THEN occupied_places ELSE 0 END) AS '2', " +
				"(CASE places WHEN '3' THEN occupied_places ELSE 0 END) AS '3', " +
				"(CASE places WHEN '4' THEN occupied_places ELSE 0 END) AS '4', " +
				"(CASE places WHEN '5' THEN occupied_places ELSE 0 END) AS '5' " +
				"FROM rooms GROUP BY rooms.roomNumber";
		List<LoadRoom> listRoom = jdbcTemplate.query(sql, new RowMapper<LoadRoom>() {

			@Override
			public LoadRoom mapRow(ResultSet rs, int rowNum) throws SQLException {
				LoadRoom aRoom = new LoadRoom();

				aRoom.setRoomNumber(rs.getInt("roomNumber"));
				//aRoom.setTotal(rs.getInt("total"));
				aRoom.setOne(rs.getInt("1"));
				aRoom.setTwo(rs.getInt("2"));
				aRoom.setThree(rs.getInt("3"));
				aRoom.setFour(rs.getInt("4"));
				aRoom.setFive(rs.getInt("5"));

				return aRoom;
			}

		});

		return listRoom;
	}
}
