package net.codejava.spring.dao;

import net.codejava.spring.model.Reserve;
import net.codejava.spring.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * An implementation of the ReserveDAO interface.
 * @author www.codejava.net
 *
 */
public class ReserveDAOImpl implements ReserveDAO {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	private VisitorDAO visitorDAO;

	@Autowired
	private RoomDAO roomDAO;

	public ReserveDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void saveOrUpdate(Reserve reserve) {
		int roomId = roomDAO.getByNumber(reserve.getRoomNumber()).getRoomId();
		int visitorId = visitorDAO.getByName(reserve.getVisitorName()).getVisitorId();
		if (reserve.getReserveId() > 0) {
			// update
			String sql = "UPDATE reservations SET dateIn=?, dateOut=?, roomId=?, "
						+ "visitorId=? WHERE reserveId=?";
			jdbcTemplate.update(sql, reserve.getDateIn(), reserve.getDateOut(),
					roomId, visitorId, reserve.getReserveId());
		} else {
			// insert

			String sql = "INSERT INTO reservations (dateIn, dateOut, roomId, visitorId)"
						+ " VALUES (?, ?, ?, ?)";
			jdbcTemplate.update(sql, reserve.getDateIn(), reserve.getDateOut(),
					roomId, visitorId);
			incOccupiedPlaces(roomId);
		}
		
	}

	public void incOccupiedPlaces(int roomNumber) {

			String sql = "UPDATE rooms SET occupied_places=?"
					+ " WHERE roomId=?";
			jdbcTemplate.update(sql, getRoom(roomNumber).getOccupiedPlaces()+1, roomNumber);
	}

	public void decOccupiedPlaces(int roomNumber) {

		String sql = "UPDATE rooms SET occupied_places=?"
				+ " WHERE roomId=?";
		jdbcTemplate.update(sql, getRoom(roomNumber).getOccupiedPlaces()-1, roomNumber);
	}

	public Room getRoom(int roomId) {
		String sql = "SELECT * FROM rooms WHERE roomId=" + roomId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Room>() {

			@Override
			public Room extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Room room = new Room();
					room.setRoomId(rs.getInt("roomId"));
					room.setRoomNumber(rs.getInt("roomNumber"));
					room.setCost(rs.getInt("cost"));
					room.setPlaces(rs.getInt("places"));
					room.setOccupiedPlaces(rs.getInt("occupied_places"));
					room.setServiceLevel(rs.getString("serviceLevel"));
					return room;
				}
				return null;
			}

		});
	}

	@Override
	public void delete(int reserveId) {
		Reserve reserve = get(reserveId);
		int roomId = roomDAO.getByNumber(reserve.getRoomNumber()).getRoomId();
		decOccupiedPlaces(roomId);
		String sql = "DELETE FROM reservations WHERE reserveId=?";
		jdbcTemplate.update(sql, reserveId);
	}

	@Override
	public List<Reserve> list() {
		String sql = "SELECT reservations.reserveId, reservations.dateIn, reservations.dateOut,  visitors.name, rooms.roomNumber " +
				"FROM reservations, visitors, rooms " +
				"where reservations.visitorId = visitors.visitorId AND rooms.roomId = reservations.roomId";
		List<Reserve> listReserve = jdbcTemplate.query(sql, new RowMapper<Reserve>() {

			@Override
			public Reserve mapRow(ResultSet rs, int rowNum) throws SQLException {
				Reserve aReserve = new Reserve();
	
				aReserve.setReserveId(rs.getInt("reserveId"));
				aReserve.setDateIn(rs.getDate("dateIn"));
				aReserve.setDateOut(rs.getDate("dateOut"));
				aReserve.setRoomNumber(rs.getInt("roomNumber"));
				aReserve.setVisitorName(rs.getString("name"));
				
				return aReserve;
			}
			
		});
		
		return listReserve;
	}

	@Override
	public Reserve get(int reserveId) {
		String sql = "SELECT reservations.reserveId, reservations.dateIn, reservations.dateOut,  visitors.name, rooms.roomNumber "
				+ "FROM reservations, visitors, rooms "
				+ "where reservations.visitorId = visitors.visitorId AND rooms.roomId = reservations.roomId AND reserveId=" + reserveId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Reserve>() {

			@Override
			public Reserve extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Reserve reserve = new Reserve();
					reserve.setReserveId(rs.getInt("reserveId"));
					reserve.setDateIn(rs.getDate("dateIn"));
					reserve.setDateOut(rs.getDate("dateOut"));
					reserve.setRoomNumber(rs.getInt("roomNumber"));
					reserve.setVisitorName(rs.getString("name"));
					return reserve;
				}
				
				return null;
			}
			
		});
	}

}
