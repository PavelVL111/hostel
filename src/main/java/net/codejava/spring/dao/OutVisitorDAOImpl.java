package net.codejava.spring.dao;

import net.codejava.spring.model.Visitor;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * An implementation of the VisitorDAO interface.
 * @author www.codejava.net
 *
 */
public class OutVisitorDAOImpl implements OutVisitorDAO {

	private JdbcTemplate jdbcTemplate;

	public OutVisitorDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Visitor> list() {
		//String sql = "SELECT visitors.* FROM visitors, reservations WHERE (visitors.visitorID = reservations.visitorID AND reservations.dateOut = CURDATE())";
		String sql = "SELECT * FROM visitors";
		List<Visitor> listVisitor = jdbcTemplate.query(sql, new RowMapper<Visitor>() {

			@Override
			public Visitor mapRow(ResultSet rs, int rowNum) throws SQLException {
				Visitor aVisitor = new Visitor();

				aVisitor.setVisitorId(rs.getInt("visitorId"));
				aVisitor.setName(rs.getString("name"));
				aVisitor.setGender(rs.getString("gender"));
				aVisitor.setTelephone(rs.getString("telephone"));

				return aVisitor;
			}

		});

		return listVisitor;
	}
}
