package net.codejava.spring.dao;

import net.codejava.spring.model.Visitor;

import java.util.List;

/**
 * Defines DAO operations for the contact model.
 * @author www.codejava.net
 *
 */
public interface OutVisitorDAO {
		public List<Visitor> list();
}
