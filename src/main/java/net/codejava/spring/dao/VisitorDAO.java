package net.codejava.spring.dao;

import net.codejava.spring.model.OutVisitor;
import net.codejava.spring.model.Visitor;

import java.util.List;

/**
 * Defines DAO operations for the contact model.
 * @author www.codejava.net
 *
 */
public interface VisitorDAO {
	
	public void saveOrUpdate(Visitor visitor);
	
	public void delete(int visitorId);
	
	public Visitor get(int visitorId);

	public Visitor getByName(String name);
	
	public List<Visitor> list();

	public List<OutVisitor> listOutVisitor();
}
