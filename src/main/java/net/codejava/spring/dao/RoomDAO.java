package net.codejava.spring.dao;

import net.codejava.spring.model.LoadRoom;
import net.codejava.spring.model.Room;

import java.util.List;

/**
 * Defines DAO operations for the contact model.
 * @author www.codejava.net
 *
 */
public interface RoomDAO {
	
	public void saveOrUpdate(Room room);
	
	public void delete(int roomId);
	
	public Room get(int roomId);

	public Room getByNumber(int roomNumber);
	
	public List<Room> list();

	public List<LoadRoom> loadRoom();
}
