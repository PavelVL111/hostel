package net.codejava.spring.dao;

import net.codejava.spring.model.OutVisitor;
import net.codejava.spring.model.Reserve;
import net.codejava.spring.model.Room;
import net.codejava.spring.model.Visitor;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * An implementation of the VisitorDAO interface.
 * @author www.codejava.net
 *
 */
public class VisitorDAOImpl implements VisitorDAO {

	private JdbcTemplate jdbcTemplate;
	
	public VisitorDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void saveOrUpdate(Visitor visitor) {
		if (visitor.getVisitorId() > 0) {
			// update
			String sql = "UPDATE visitors SET name=?, gender=?, "
						+ "telephone=? WHERE visitorId=?";
			jdbcTemplate.update(sql, visitor.getName(), visitor.getGender(),
					visitor.getTelephone(), visitor.getVisitorId());
		} else {
			// insert
			String sql = "INSERT INTO visitors (name, gender, telephone)"
						+ " VALUES (?, ?, ?)";
			jdbcTemplate.update(sql, visitor.getName(), visitor.getGender(),
					visitor.getTelephone());
		}
		
	}


	@Override
	public void delete(int visitorId) {
		Reserve reserve = null;
		try {
			reserve = getReserve(visitorId);
			int roomId = reserve.getRoomNumber();
			decOccupiedPlaces(roomId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String sql2 = "DELETE FROM reservations WHERE visitorId=?";
		jdbcTemplate.update(sql2, visitorId);
		String sql = "DELETE FROM visitors WHERE visitorId=?";
		jdbcTemplate.update(sql, visitorId);
	}

	public Reserve getReserve(int visitorId) throws Exception {
		String sql = "SELECT * FROM reservations WHERE visitorId=" + visitorId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Reserve>() {

			@Override
			public Reserve extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Reserve reserve = new Reserve();
					reserve.setReserveId(rs.getInt("reserveId"));
					reserve.setDateIn(rs.getDate("dateIn"));
					reserve.setDateOut(rs.getDate("dateOut"));
					reserve.setRoomNumber(rs.getInt("roomId"));
					reserve.setVisitorName(rs.getString("visitorId"));
					return reserve;
				}

				return null;
			}

		});
	}

	public void decOccupiedPlaces(int roomNumber) {

		String sql = "UPDATE rooms SET occupied_places=?"
				+ " WHERE roomId=?";
		jdbcTemplate.update(sql, getRoom(roomNumber).getOccupiedPlaces()-1, roomNumber);
	}

	public Room getRoom(int roomId) {
		String sql = "SELECT * FROM rooms WHERE roomId=" + roomId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Room>() {

			@Override
			public Room extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Room room = new Room();
					room.setRoomId(rs.getInt("roomId"));
					room.setRoomNumber(rs.getInt("roomNumber"));
					room.setCost(rs.getInt("cost"));
					room.setPlaces(rs.getInt("places"));
					room.setOccupiedPlaces(rs.getInt("occupied_places"));
					room.setServiceLevel(rs.getString("serviceLevel"));
					return room;
				}
				return null;
			}

		});
	}

	@Override
	public List<Visitor> list() {
		String sql = "SELECT * FROM visitors";
		List<Visitor> listVisitor = jdbcTemplate.query(sql, new RowMapper<Visitor>() {

			@Override
			public Visitor mapRow(ResultSet rs, int rowNum) throws SQLException {
				Visitor aVisitor = new Visitor();
	
				aVisitor.setVisitorId(rs.getInt("visitorId"));
				aVisitor.setName(rs.getString("name"));
				aVisitor.setGender(rs.getString("gender"));
				aVisitor.setTelephone(rs.getString("telephone"));
				
				return aVisitor;
			}
			
		});
		
		return listVisitor;
	}

	@Override
	public Visitor get(int visitorId) {
		String sql = "SELECT * FROM visitors WHERE visitorId=" + visitorId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Visitor>() {

			@Override
			public Visitor extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Visitor visitor = new Visitor();
					visitor.setVisitorId(rs.getInt("visitorId"));
					visitor.setName(rs.getString("name"));
					visitor.setGender(rs.getString("gender"));
					visitor.setTelephone(rs.getString("telephone"));
					return visitor;
				}
				
				return null;
			}
			
		});
	}

	@Override
	public Visitor getByName(String name) {
		String sql = "SELECT * FROM visitors WHERE name='" + name +"'";
		return jdbcTemplate.query(sql, new ResultSetExtractor<Visitor>() {

			@Override
			public Visitor extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Visitor visitor = new Visitor();
					visitor.setVisitorId(rs.getInt("visitorId"));
					visitor.setName(rs.getString("name"));
					visitor.setGender(rs.getString("gender"));
					visitor.setTelephone(rs.getString("telephone"));
					return visitor;
				}

				return null;
			}

		});
	}
	@Override
	public List<OutVisitor> listOutVisitor() {
		String sql = "SELECT visitors.*, (reservations.dateOut - reservations.dateIn) * rooms.cost AS costsum FROM visitors, reservations, rooms WHERE (visitors.visitorID = reservations.visitorID AND reservations.dateOut = CURDATE() AND rooms.roomId = reservations.roomId)";

		List<OutVisitor> listVisitor = jdbcTemplate.query(sql, new RowMapper<OutVisitor>() {
			@Override
			public OutVisitor mapRow(ResultSet rs, int rowNum) throws SQLException {
				OutVisitor aVisitor = new OutVisitor();

				aVisitor.setVisitorId(rs.getInt("visitorId"));
				aVisitor.setName(rs.getString("name"));
				aVisitor.setGender(rs.getString("gender"));
				aVisitor.setTelephone(rs.getString("telephone"));
				aVisitor.setCost(rs.getInt("costsum"));

				return aVisitor;
			}

		});

		return listVisitor;
	}
}
