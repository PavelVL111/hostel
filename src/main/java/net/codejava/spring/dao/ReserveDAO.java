package net.codejava.spring.dao;

import net.codejava.spring.model.Reserve;

import java.util.List;

/**
 * Defines DAO operations for the contact model.
 * @author www.codejava.net
 *
 */
public interface ReserveDAO {
	
	public void saveOrUpdate(Reserve reserve);
	
	public void delete(int reserveId);
	
	public Reserve get(int reserveId);
	
	public List<Reserve> list();
}
