create database contactdb;

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8

CREATE TABLE `visitors` (
  `visitorId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `photo` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  PRIMARY KEY (`visitorId`)
) AUTO_INCREMENT=25 DEFAULT CHARSET=utf8

CREATE TABLE `rooms` (
  `roomId` int(11) NOT NULL AUTO_INCREMENT,
  `roomNumber` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `serviceLevel` varchar(45) NOT NULL,
  PRIMARY KEY (`roomId`)
) AUTO_INCREMENT=25 DEFAULT CHARSET=utf8

CREATE TABLE `reservations` (
  `reserveId` int(11) NOT NULL AUTO_INCREMENT,
  `dateIn` DATE NOT NULL,
  `dateOut` DATE NOT NULL,
  `roomId` int(11) NOT NULL,
  `visitorId` int(11) NOT NULL,
  PRIMARY KEY (`reserveId`),
    FOREIGN KEY (`roomId`) REFERENCES `rooms` (`roomId`),
    FOREIGN KEY (`visitorId`) REFERENCES `visitors` (`visitorId`)
) AUTO_INCREMENT=25 DEFAULT CHARSET=utf8